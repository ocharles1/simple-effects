{-# LANGUAGE RecordWildCards, NamedFieldPuns, NoMonomorphismRestriction #-}
{-# LANGUAGE CPP #-}
module Control.Effects.Plugin
  ( plugin )
where

-- external
import GHC.TcPluginM.Extra (lookupModule, lookupName)
import Data.Generics.Schemes

-- GHC API
import FastString (fsLit)
import Module     (mkModuleName)
import OccName    (mkTcOcc)
import Plugins    (Plugin (..), defaultPlugin
#if __GLASGOW_HASKELL__ >= 806
    , PluginRecompile(..)
#endif
    )
import TcPluginM  (TcPluginM, tcLookupClass)
import TcRnTypes
import TyCoRep    (Type (..))
import Control.Monad
import Class
import Type
import Data.Maybe
import TcSMonad hiding (tcLookupClass)
import CoAxiom

plugin :: Plugin
plugin = defaultPlugin
    { tcPlugin = const (Just fundepPlugin)
#if __GLASGOW_HASKELL__ >= 806
    , pluginRecompile = const (return NoForceRecompile)
#endif
    }

fundepPlugin :: TcPlugin
fundepPlugin = TcPlugin
    { tcPluginInit = do
        md <- lookupModule (mkModuleName "Control.Effects") (fsLit "simple-effects")
        monadEffectTcNm <- lookupName md (mkTcOcc "MonadEffect")
        tcLookupClass monadEffectTcNm
    , tcPluginSolve = solveFundep
    , tcPluginStop = const (return ()) }

allMonadEffectConstraints :: Class -> [Ct] -> [(CtLoc, (Type, Type, Type))]
allMonadEffectConstraints cls cts =
    [ (ctLoc cd, (effName, eff, mon))
        | cd@CDictCan{cc_class = cls', cc_tyargs = [eff, mon]} <- cts
        , cls == cls'
        , let (effName, args) = splitAppTys eff
        , any isTyVarTy args
        ]

singleListToJust :: [a] -> Maybe a
singleListToJust [a] = Just a
singleListToJust _ = Nothing

findMatchingEffectIfSingular :: (Type, Type, Type) -> [(Type, Type, Type)] -> Maybe Type
findMatchingEffectIfSingular (_, args, mon) ts = singleListToJust
    [ eff'
        | (_, eff', mon') <- ts
        , eqUptoTyVars args eff'
        , eqType mon mon' ]

eqUptoTyVars :: Type -> Type -> Bool
eqUptoTyVars x y =
  let (h1, args1) = splitAppTys x
      (h2, args2) = splitAppTys y
      eqUptoTyVar a b =
        isTyVarTy a || isTyVarTy b || eqType a b
  in
  eqType h1 h2
    && and (zipWith eqUptoTyVar args1 args2)
    && length args1 == length args2

solveFundep :: Class -> [Ct] -> [Ct] -> [Ct] -> TcPluginM TcPluginResult
solveFundep effCls giv _ want = do
    let wantedEffs = allMonadEffectConstraints effCls want
    let givenEffs = snd <$> allMonadEffectConstraints effCls giv
    -- Match wanted MonadEffect constraints (created during type checking the
    -- body of a monadic expression) against given MonadEffect constraints
    -- (usually in the type signature).
    eqs <- forM wantedEffs $ \(loc, e@(_, eff, _)) ->
        case findMatchingEffectIfSingular e givenEffs of
            Nothing -> return Nothing
            Just eff' -> do
                (ev, _) <- unsafeTcPluginTcM
                    (runTcSDeriveds (newWantedEq loc Nominal eff eff'))
                return (Just (CNonCanonical ev))

    -- Generate equality constraints for all wanted constraints using the
    -- knowledge of the monad that we're in. For example, if you have something
    -- like 'implement ops.. someOp', then the "someOp" call generates a wanted
    -- constraint where mon ~ RuntimeImplemented (Effect a b c). Thus we know
    -- that whatever MonadEffect is wanted for "Effect", it should equal
    -- MonadEffect (Effect a b c) (as that is how we're going to run it).
    monEqs <-
      forM wantedEffs $ \(loc, (_, eff, mon)) ->
        let concrete = listify (eqUptoTyVars eff) mon in
        forM concrete $ \eff' -> do
          (ev, _) <- unsafeTcPluginTcM
              (runTcSDeriveds (newWantedEq loc Nominal eff eff'))
          return (Just (CNonCanonical ev))

    return (TcPluginOk [] (catMaybes (eqs ++ concat monEqs)))
